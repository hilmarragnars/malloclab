/* mm.c
 *
 * We are using Explicit Free list in our implementation. We are using the
 * first-fit algorithm.
 * 
 * The Explicit list is going to be implemented with doubly linked list.
 * We are going to use LIFO in our implementation.
 *
 * Freeing:
 * When we free a block we put it in the front of the list. We change 
 * the allocation bit. When a newly freed block is next to a other free
 * block we must coalesce the blocks.
 *
 * Mallocing:
 * Block is allocated going through the free list and deleting the
 * first free block that has the size that fits for requested size.
 * If the free block we found is to big then we are going to
 * split that block if the remainig part is big enough to be 
 * a free block. We delete the first free block and fix the header and footer
 * on the latter one. 
 *
 * Reallocing:
 * If increase size, call malloc with the new size and free the old block
 * If Decrease size, shrink the block and change the information in
 * in the header and footer.
 * If the size is the same then we keep the block.
 *
 * How the blocks are build
 * free blocks 		|header|prev|next|old data|footer|
 * allocated blocks	|header|       data       |footer|
 *
 * Hoe the heap is build.
 *
 * begin                                                          end
 * heap                                                           heap  
 *  -----------------------------------------------------------------   
 * |  pad   | hdr(8:a) | ftr(8:a) | zero or more usr blks | hdr(8:a) |
 *  -----------------------------------------------------------------
 *          |       prologue      |                       | epilogue |
 *          |         block       |                       | block    |
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * === User information ===
 * Group: Erkimoddarar 
 * User 1: sindrik13
 * SSN: 220592-2359
 * User 2: hilmarr13
 * SSN: 231092-2509
 * === End User Information ===
 *********************************************************/
team_t team = {
    /* Group name */
    "Erkimoddarar",
    /* First member's full name */
    "Sindri Mar Kaldal Sigurjonsson",
    /* First member's email address */
    "sindrik13@ru.is",
    /* Second member's full name (leave blank if none) */
    "Hilmar Ragnarsson",
    /* Second member's email address (leave blank if none) */
    "hilmarr13@ru.is",
    /* Leave blank */
    "",
    /* Leave blank */
    ""
};

/* $begin mallocmacros */
/* Basic constants and macros */
#define WSIZE       4       /* word size (bytes) */  
#define DSIZE       8       /* doubleword size (bytes) */
#define CHUNKSIZE  (1<<12)  /* initial heap size (bytes) */
#define OVERHEAD    8       /* overhead of header and footer (bytes) */

#define MAX(x, y) ((x) > (y)? (x) : (y))  

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc)  ((size) | (alloc))

/* Read and write a word at address p */
#define GET(p)       (*(size_t *)(p))
#define PUT(p, val)  (*(size_t *)(p) = (val))  

/* (which is about 54/100).* Read the size and allocated fields from address p */
#define GET_SIZE(p)  (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x1)

/* Given block ptr bp, compute address of its header and footer */
#define PREV_FREEP(bp)      (*(char **)((bp) + WSIZE))
#define NEXT_FREEP(bp)      (*(char **)(bp))
#define HDRP(bp)       ((char *)(bp) - WSIZE)  
#define FTRP(bp)       ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE)

/* Given block ptr bp, compute address of next and previous blocks */
#define NEXT_BLKP(bp)  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)))
#define PREV_BLKP(bp)  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)))
/* $end mallocmacros */

/*#ifdef DEBUG
#define CHECKHEAP(verbose) printf("%s\n", __func__); mm_checkheap(verbose);
#endif*/

/* Global variables */
static char *heap_listp;  /* pointer to first block */  
static char *free_listp;  /* pinter to first free block */
static int printDebug = 0; /* Makes startFunc and endFunc active or not */
static int ch = 0; /* makes mm_checkheap active or not */

/* functions, internal helper routines */
static void remove_allocated(char* bp);
static void *extend_heap(size_t words);
static void place(void *bp, size_t asize);
static void *find_fit(size_t asize);
static void *coalesce(void *bp);
static void printblock(void *bp); 
static void checkblock(void *bp);
static void startFunc(char* funcName);
static void endFunc(char* funcName);
static void mm_checkheap(int verbose);
/* 
 * mm_init - Initialize the memory manager 
 */
/* $begin mminit */
int mm_init(void) 
{
   if(printDebug) {
	 startFunc("init");
   }

    /* create the initial empty heap */
    if ((heap_listp = mem_sbrk(4*WSIZE)) == NULL)
        return -1;
    PUT(heap_listp, 0);                        /* alignment padding */
    PUT(heap_listp+WSIZE, PACK(OVERHEAD, 1));  /* prologue header */ 
    PUT(heap_listp+DSIZE, PACK(OVERHEAD, 1));  /* prologue footer */ 
    PUT(heap_listp+WSIZE+DSIZE, PACK(0, 1));   /* epilogue header */
    heap_listp += DSIZE;


    free_listp = NULL; 
    /* Extend the empty heap with a free block of CHUNKSIZE bytes */
    if (extend_heap(CHUNKSIZE/DSIZE) == NULL)
        return -1;

   if(printDebug) {
      endFunc("init");
   }
    return 0;
}
/* $end mminit */

/* 
 * mm_malloc - Allocate a block with at least size bytes of payload 
 */
/* $begin mmmalloc */
void *mm_malloc(size_t size) 
{
    if(printDebug) {  
	startFunc("mm_malloc");
    }

    size_t asize;      /* adjusted block size */
    size_t extendsize; /* amount to extend heap if no fit */
    void *bp;      

    /* Ignore spurious requests */
    if (size <= 0)
        return NULL;

    /* Adjust block size to include overhead and alignment reqs. */
    if (size <= DSIZE)
        asize = DSIZE + OVERHEAD;
    else
        asize = DSIZE * ((size + (OVERHEAD) + (DSIZE-1)) / DSIZE);
    
    
    /* Search the free list for a fit */
    if ((bp = find_fit(asize)) != NULL) {
        place(bp, asize);
        return bp;
    }

    /* No fit found. Get more memory and place the block */
    extendsize = MAX(asize,CHUNKSIZE);
    if ((bp = extend_heap(extendsize/WSIZE)) == NULL)
        return NULL;
    place(bp, asize);

    if(printDebug) {  
	endFunc("mm_malloc");
    }

    return bp;
} 
/* $end mmmalloc */

/* 
 * mm_free - Free a block 
 */
/* $begin mmfree */
void mm_free(void *bp)
{
    if(printDebug) {   
	startFunc("mm_free");
    }
    
    //Fetch the size of the block
    size_t size = GET_SIZE(HDRP(bp));
 
    //Assign the size to the header and footer of the block and free it
    PUT(HDRP(bp), PACK(size, 0));
    PUT(FTRP(bp), PACK(size, 0));

    //Coalesce the free block and put it to the front of the list
    coalesce(bp);

    if(printDebug) {
        endFunc("mm_free");
    }
}

/* $end mmfree */

/*
 * mm_realloc. Takes a pointer as an argument that points to a block in the heap.
 * Also takes a size in as an argument that represents the new requested size of 
 * the block. We then find a new free block that fits the new size.
 */
/* $begin mmrealloc */
void *mm_realloc(void *ptr, size_t size)
{
    if(printDebug) {
        startFunc("mm_realloc");
    }

    /*If we have a negative we return NULL beacuse we can't realloc a block of negative size */
    if(size < 0) {
        return NULL;
    }

    /*If the size is zero we free the block and return NULL */
    if(size == 0) {
        mm_free(ptr);
        return NULL;
    }
    void *newp;
    size_t copySize = GET_SIZE(HDRP(ptr));
    size_t newSize = size + 2 * WSIZE;
    
    /*If the new adjusted size is smaller than the size of the block we just return the pointer */
    if (newSize <= copySize) {
        return ptr;
    }
    
    /*Variables the tell us if the blocks on both sides of the block are allocated or not*/
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(ptr)));

    /*If the next block is free and the combined size of the blocks is larger than
    * the requested size then we combine those two blocks, thus improving the util */
    if(!next_alloc && (copySize + GET_SIZE(HDRP(NEXT_BLKP(ptr)))) >= newSize){
        size_t comb_size = copySize + GET_SIZE(HDRP(NEXT_BLKP(ptr)));
        remove_allocated(NEXT_BLKP(ptr));
        PUT(HDRP(ptr), PACK(comb_size, 1)); 
        PUT(FTRP(ptr), PACK(comb_size, 1));
        return ptr; 
    }
    
    /*If nothing above applies then we call mm_malloc, get a new pointer that
    *points to the new free block. Copy the payload from ptr to newp and
    *then we free the block that ptr points to. */
    if ((newp = mm_malloc(newSize)) == NULL) {
        printf("ERROR: mm_malloc failed in mm_realloc\n");                                                                       
        exit(1);                                                                                                                 
    }                                                                                                                      
    if(ptr == NULL) { 
        return newp;       
    }                                                                                                                             
    memcpy(newp, ptr, newSize);                                                                                        
                                                                                                                
    mm_free(ptr);                                             
                                                                                                                           
    if(printDebug) {
       endFunc("mm_realloc");   
    }                                                                                                          

    return newp;                                                                                                                 
}  
/* $end mmrealloc */

/* 
 * mm_checkheap - Check the heap for consistency 
 */

/* $begin mmcheckheap */
void mm_checkheap(int verbose) 
{
    if(printDebug) {
        startFunc("mm_checkheap");
    }

    char *bp = heap_listp;
    char *ptr = heap_listp;

    /*Check if every block in the free list is marked free
    *and prints out the free list.*/
    if (verbose){
	printf("This is the free list!\n");
	
   	for (bp = free_listp; bp != NULL; bp = NEXT_FREEP(bp)) {
		if(GET_ALLOC(HDRP(bp))){
		    printf("There is a block in free list that is not FREE!!");
		}
   	        printblock(bp); 
	}
	printf("end the free list\n");
    }	
    if (verbose)
        printf("Heap (%p):\n", heap_listp);

    if ((GET_SIZE(HDRP(heap_listp)) != DSIZE) || !GET_ALLOC(HDRP(heap_listp)))
        printf("Bad prologue header\n");
    checkblock(heap_listp);

    //Prints out the heap and checks if every free block in the heap is in
    //the free list.
    for (bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
        if (verbose)
	    if(GET_ALLOC(HDRP(bp)) == 0){
   		for (ptr = free_listp; ptr != NULL; ptr = NEXT_FREEP(ptr)) {
		    if(ptr == bp){
			break;
		    }
		}
		if(ptr != bp){
		    printf("There is a free block in the heap that is not in the free list!!\n");
		    exit(0);
		}		
	    }
            printblock(bp);
        checkblock(bp);
    }
  
    if (verbose)
        printblock(bp);
    if ((GET_SIZE(HDRP(bp)) != 0) || !(GET_ALLOC(HDRP(bp))))
        printf("Bad epilogue header\n");

    if(printDebug) {
        endFunc("mm_checkheap");
    }
}
/* $end mmcheckheap */

/* The remaining routines are internal helper routines */

/* 
 * extend_heap - Extend heap with free block and return its block pointer
 */
/* $begin mmextendheap */
static void *extend_heap(size_t words) 
{
    if(printDebug) { 
	startFunc("extend_heap");
    }

    char *bp;
    size_t size;
        
    /* Allocate an even number of words to maintain alignment */
    size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
    if ((bp = mem_sbrk(size)) == (void *)-1) 
        return NULL;

    /* Initialize free block header/footer and the epilogue header */
    PUT(HDRP(bp), PACK(size, 0));         /* free block header */
    PUT(FTRP(bp), PACK(size, 0));         /* free block footer */
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* new epilogue header */

    if(printDebug) {  
	endFunc("extend_heap");
    }

    /* Coalesce if the previous block was free */
    return coalesce(bp);
}
/* $end mmextendheap */

/* 
 * place - Place block of asize bytes at start of free block bp 
 *         and split if remainder would be at least minimum block size
 */
/* $begin mmplace */
static void place(void *bp, size_t asize)
{
    if(printDebug) { 
	startFunc("place");
    }

    /*Get size of the block we are planning to allocate*/
    size_t csize = GET_SIZE(HDRP(bp));

    /*If the block if big enough allocate another block we split the block
     *and then allocate the given size. */

    if ((csize - asize) >= (DSIZE + OVERHEAD)) {
	/*Start by allocating the given size and place the size in the header and
	 *the footer */
        PUT(HDRP(bp), PACK(asize, 1));
        PUT(FTRP(bp), PACK(asize, 1));
       	
	/*A helper function to remove the newly allocated block from the 
	 *free list */
        remove_allocated(bp);
       
	/*Move the pointer to the next block and asign the remaining size of 
	 *the block to it's header and footer */
        bp = NEXT_BLKP(bp);
        PUT(HDRP(bp), PACK(csize-asize, 0));
        PUT(FTRP(bp), PACK(csize-asize, 0));
        
	/*Coalesce the free block and put it to the front of the list */
        coalesce(bp);
    }
    /*If the block is not big enough to fit another allocated block we just allocate it
     *and remove it from the free list */
    else { 
        PUT(HDRP(bp), PACK(csize, 1));
        PUT(FTRP(bp), PACK(csize, 1));
        remove_allocated(bp);
    }

    if(printDebug) {  
	endFunc("place");
    }
}
/* $end mmplace */

/* 
 * find_fit - Find a fit for a block with asize bytes 
 */
/* $begin mmfind_fit */
static void *find_fit(size_t asize)
{
    if(printDebug) { 
	startFunc("fint_fit");
    }

    if(free_listp == NULL){
	return NULL;
    }

    /* first fit search */
    char *bp;
    
    /*a loop iterates through the free list to find a free block that fits*/
    for (bp = free_listp; bp != NULL; bp = NEXT_FREEP(bp)) 
    {
        if (!GET_ALLOC(HDRP(bp)) && asize <= GET_SIZE(HDRP(bp))) {
            return bp;
        }
    }

    if(printDebug) {  
	endFunc("fint_fit");
    }

    return NULL; /* no fit */
}
/* $end mmfind_fit */

/*
 * coalesce - boundary tag coalescing. Return ptr to coalesced block
 */ 
/* $begin mmcoalesce */ 
static void *coalesce(void *bp) 
{
    if(printDebug) { 
	startFunc("coalesce");
    }

    /*These variables tell us if the blocks on both sides of the newly freed blcok
     *are allocated or not */
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));

    /*Fetch the size of the block */
    size_t size = GET_SIZE(HDRP(bp));

    /*if the prev block is allocated and the next block is free*/
    if (prev_alloc && !next_alloc) {     	 /* Case 1 */
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
	
	/*We remove the free block from the free list and then we 
 	*coalesce that free block to the new free block 
	*so that we have one large free block. We do this in every case*/
	remove_allocated(NEXT_BLKP(bp));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size,0));
    }

    /*if the prev block is free and the next block is allocated*/
    else if (!prev_alloc && next_alloc) {      	/* Case 2 */
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
	remove_allocated(PREV_BLKP(bp));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }

    /*if blocks on both sides are free*/
    else if (!prev_alloc && !next_alloc){  	 /* Case 3 */
        size += GET_SIZE(HDRP(PREV_BLKP(bp))) + 
        GET_SIZE(FTRP(NEXT_BLKP(bp)));
	remove_allocated(NEXT_BLKP(bp));
	remove_allocated(PREV_BLKP(bp));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }
    
    /* Put the newly freed block to the front of the free-block list
    *if the list is empty, the free_listp point on bp*/
    if(free_listp == NULL){
	NEXT_FREEP(bp) = NULL;
	PREV_FREEP(bp) = NULL;
	free_listp = bp;
    }

    /*If the list is not empty 	we use the decreasing design for the free list. That is, we always put the
     *largest block to the front of the list and then put the new block in the correct place in the list
     *according to it's size  */
    else {
	 char* p;
	 for(p = free_listp; p != NULL; p = NEXT_FREEP(p)) {
		if(GET_SIZE(HDRP(p)) <= GET_SIZE(HDRP(bp))) {
		    if(PREV_FREEP(p) == NULL) {
			NEXT_FREEP(bp) = free_listp;
   	 		PREV_FREEP(free_listp) = bp;
   	 		PREV_FREEP(bp) = NULL;
   	 		free_listp = bp;
		    }
		    else {
		        NEXT_FREEP(PREV_FREEP(p)) = bp;
		        PREV_FREEP(bp) = PREV_FREEP(p);
		        NEXT_FREEP(bp) = p;
		        PREV_FREEP(p) = bp;
		    }
		    return bp;
		}
		else if(NEXT_FREEP(p) == NULL) {
		    PREV_FREEP(bp) = p;
		    NEXT_FREEP(p) = bp;
		    NEXT_FREEP(bp) = NULL;
		    return bp;
		}
	}
    }
    if(printDebug) { 
	endFunc("coalencse");
    }

    return bp;
    
}
/* $end mmcoalesce */

/*This is a function to print a chosen block, convenient when looking at the heap */

/* $begn printblock */
static void printblock(void *bp) 
{
    size_t hsize, halloc, fsize, falloc;

    hsize = GET_SIZE(HDRP(bp));
    halloc = GET_ALLOC(HDRP(bp));  
    fsize = GET_SIZE(FTRP(bp));
    falloc = GET_ALLOC(FTRP(bp));  
    
    if (hsize == 0) {
        printf("%p: EOL\n", bp);
        return;
    }

    printf("%p: header: [%d:%c] footer: [%d:%c]\n", bp, 
           hsize, (halloc ? 'a' : 'f'), 
           fsize, (falloc ? 'a' : 'f')); 
}
/* $end printblock */

/*Romoving an allocated block from the free list
* $begin remove_allocated */
static void remove_allocated(char* bp)
{
    /*If both the prev pointer and next pointer are null that means we only have a single
     * free block and thus just assign NULL to free_listp  */	
    if(PREV_FREEP(bp) == NULL && NEXT_FREEP(bp) == NULL){
    	free_listp = NULL; 
    }

    /*Else if only the prev pointer is NULL, that means we have are looking at the first block in the 
     *free list and thus just move the free_listp to the next block and assign the old block's header to 
     *NULL and the prev pointer of the new first block to NULL*/
    else if(PREV_FREEP(bp) == NULL) {
	free_listp = NEXT_FREEP(bp);
	PREV_FREEP(NEXT_FREEP(bp)) = NULL;
	NEXT_FREEP(bp) = NULL;
    }
    /*else if only the next pointer is NULL that means we are looking at the last block in the free list and thus
     *have to just set the next pointer of the previous block to NULL  */
    else if(NEXT_FREEP(bp) == NULL){
	NEXT_FREEP(PREV_FREEP(bp)) = NULL;
	PREV_FREEP(bp) = NULL;
    } 
    /*If the block fits none of the above cases we have a block in the middle of the list and thus just connect
     *the two block on each side of the block */
    else {
	PREV_FREEP(NEXT_FREEP(bp)) = PREV_FREEP(bp);
        NEXT_FREEP(PREV_FREEP(bp)) = NEXT_FREEP(bp);
    }
}
/* $end remove_allocated */

/* $begin mmcheckblock */
static void checkblock(void *bp) 
{
    /*Check the block is aligned to 8 and if the header matches the footer*/
    if ((size_t)bp % 8)
        printf("Error: %p is not doubleword aligned\n", bp);
    if (GET(HDRP(bp)) != GET(FTRP(bp)))
        printf("Error: header does not match footer\n");
}
/* $end mmcheckblock */

/*print out a start message when a function starts*/
/* $begin mmstartFunc */
static void startFunc(char* funcName){

    printf("-----------START: %s------------\n", funcName);
}
/* $end mmstartFunc */

/*print out a end message when a function ends*/
/* $begin mmendFunc */
static void endFunc(char* funcName){

    printf("-----------END: %s------------\n", funcName);    
}
/* $end mmendFunc */
